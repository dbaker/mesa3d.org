---
title:    "Mesa 7.11 (final) is released"
date:     2011-07-31 00:00:00
category: releases
tags:     []
---
[Mesa 7.11](https://docs.mesa3d.org/relnotes/7.11.html) (final) is released. This is a new
development release.
