---
title:    "Mesa 19.0.6 is released"
date:     2019-06-05 00:00:00
category: releases
tags:     []
---
[Mesa 19.0.6](https://docs.mesa3d.org/relnotes/19.0.6.html) is released. This is a bug-fix
release.
