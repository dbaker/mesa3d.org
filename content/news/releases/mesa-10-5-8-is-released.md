---
title:    "Mesa 10.5.8 is released"
date:     2015-06-20 00:00:00
category: releases
tags:     []
---
[Mesa 10.5.8](https://docs.mesa3d.org/relnotes/10.5.8.html) is released. This is a bug-fix
release.
