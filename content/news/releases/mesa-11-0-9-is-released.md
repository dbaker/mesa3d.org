---
title:    "Mesa 11.0.9 is released"
date:     2016-01-22 00:00:00
category: releases
tags:     []
summary:  "[Mesa 11.0.9](https://docs.mesa3d.org/relnotes/11.0.9.html) is released. This is a bug-fix
release."
---
[Mesa 11.0.9](https://docs.mesa3d.org/relnotes/11.0.9.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 11.0.9 will be the final release in the
11.0 series. Users of 11.0 are encouraged to migrate to the 11.1 series
in order to obtain future fixes.
{{< /alert >}}
