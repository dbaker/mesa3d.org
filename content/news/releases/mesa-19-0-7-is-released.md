---
title:    "Mesa 19.0.7 is released"
date:     2019-06-24 00:00:00
category: releases
tags:     []
summary:  "[Mesa 19.0.7](https://docs.mesa3d.org/relnotes/19.0.7.html) is released. This is a bug-fix
release."
---
[Mesa 19.0.7](https://docs.mesa3d.org/relnotes/19.0.7.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
It is anticipated that 19.0.7 will be the final release in the
19.0 series. Users of 19.0 are encouraged to migrate to the 19.1 series
in order to obtain future fixes.
{{< /alert >}}
