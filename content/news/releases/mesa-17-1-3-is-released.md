---
title:    "Mesa 17.1.3 is released"
date:     2017-06-19 00:00:00
category: releases
tags:     []
---
[Mesa 17.1.3](https://docs.mesa3d.org/relnotes/17.1.3.html) is released. This is a bug-fix
release.
