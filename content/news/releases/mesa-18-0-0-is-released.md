---
title:    "Mesa 18.0.0 is released"
date:     2018-03-27 00:00:00
category: releases
tags:     []
---
[Mesa 18.0.0](https://docs.mesa3d.org/relnotes/18.0.0.html) is released. This is a new
development release. See the release notes for more information about
the release.
