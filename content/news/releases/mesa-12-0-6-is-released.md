---
title:    "Mesa 12.0.6 is released"
date:     2017-01-23 00:00:00
category: releases
tags:     []
summary:  "[Mesa 12.0.6](https://docs.mesa3d.org/relnotes/12.0.6.html) is released. This is a bug-fix
release."
---
[Mesa 12.0.6](https://docs.mesa3d.org/relnotes/12.0.6.html) is released. This is a bug-fix
release.

{{< alert type="info" title="Note" >}}
This is an extra release for the 12.0 stable branch, as per
developers' feedback. It is anticipated that 12.0.6 will be the final
release in the 12.0 series. Users of 12.0 are encouraged to migrate to
the 13.0 series in order to obtain future fixes.
{{< /alert >}}
