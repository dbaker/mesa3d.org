---
title:    "Mesa 17.0.6 is released"
date:     2017-05-12 00:00:00
category: releases
tags:     []
---
[Mesa 17.0.6](https://docs.mesa3d.org/relnotes/17.0.6.html) is released. This is a bug-fix
release.
