---
title:    "Mesa 18.3.2 is released"
date:     2019-01-17 00:00:00
category: releases
tags:     []
---
[Mesa 18.3.2](https://docs.mesa3d.org/relnotes/18.3.2.html) is released. This is a bug-fix
release.
