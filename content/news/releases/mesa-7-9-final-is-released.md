---
title:    "Mesa 7.9 (final) is released"
date:     2010-10-04 00:00:00
category: releases
tags:     []
---
[Mesa 7.9](https://docs.mesa3d.org/relnotes/7.9.html) (final) is released. This is a new
development release.
