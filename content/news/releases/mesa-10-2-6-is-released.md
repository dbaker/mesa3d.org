---
title:    "Mesa 10.2.6 is released"
date:     2014-08-19 00:00:00
category: releases
tags:     []
---
[Mesa 10.2.6](https://docs.mesa3d.org/relnotes/10.2.6.html) is released. This is a bug-fix
release.
