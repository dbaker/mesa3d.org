---
title:    "Mesa 18.1.2 is released"
date:     2018-06-15 00:00:00
category: releases
tags:     []
---
[Mesa 18.1.2](https://docs.mesa3d.org/relnotes/18.1.2.html) is released. This is a bug-fix
release.
