---
title:    "Mesa 17.3.1 is released"
date:     2017-12-21 00:00:00
category: releases
tags:     []
---
[Mesa 17.3.1](https://docs.mesa3d.org/relnotes/17.3.1.html) is released. This is a bug-fix
release.
