---
title:    "Mesa 10.3.5 is released"
date:     2014-12-05 00:00:00
category: releases
tags:     []
---
[Mesa 10.3.5](https://docs.mesa3d.org/relnotes/10.3.5.html) is released. This is a bug-fix
release.
