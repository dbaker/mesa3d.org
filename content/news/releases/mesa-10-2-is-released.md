---
title:    "Mesa 10.2 is released"
date:     2014-06-06 00:00:00
category: releases
tags:     []
summary:  "[Mesa 10.2](https://docs.mesa3d.org/relnotes/10.2.html) is released. This is a new development
release. See the release notes for more information about the release."
---
[Mesa 10.2](https://docs.mesa3d.org/relnotes/10.2.html) is released. This is a new development
release. See the release notes for more information about the release.

Also, [Mesa 10.1.5](https://docs.mesa3d.org/relnotes/10.1.5.html) is released. This is a bug
fix release from the 10.1 branch.
