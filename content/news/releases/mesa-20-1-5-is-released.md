---
title:    "Mesa 20.1.5 is released"
date:     2020-08-05 23:01:29
category: releases
tags:     []
---
[Mesa 20.1.5](https://docs.mesa3d.org/relnotes/20.1.5.html) is released. This is a bug fix release.
