---
title:    "Mesa 18.2.7 is released"
date:     2018-12-13 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.7](https://docs.mesa3d.org/relnotes/18.2.7.html) is released. This is a bug-fix
release.
