---
title:    "Mesa 9.1.5 is released"
date:     2013-07-17 00:00:00
category: releases
tags:     []
---
[Mesa 9.1.5](https://docs.mesa3d.org/relnotes/9.1.5.html) is released. This is a bug fix
release.
