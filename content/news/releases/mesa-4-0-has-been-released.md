---
title:    "Mesa 4.0 has been released"
date:     2001-10-22 00:00:00
category: releases
tags:     []
summary:  "Mesa 4.0 has been released. This is a stable release."
---
Mesa 4.0 has been released. This is a stable release.

``` 
    New:
      - Mesa 4.0 implements the OpenGL 1.3 specification
      - GL_IBM_rasterpos_clip extension
      - GL_EXT_texture_edge_clamp extension (aka GL_SGIS_texture_edge_clamp)
      - GL_ARB_texture_mirrored_repeat extension
      - WindML UGL driver (Stephane Raimbault)
      - added OSMESA_MAX_WIDTH/HEIGHT queries
      - attempted compiliation fixes for Solaris 5, 7 and 8
      - updated glext.h and glxext.h files
      - updated Windows driver (Karl Schultz)
    Bug fixes:
      - added some missing GLX 1.3 tokens to include/GL/glx.h
      - GL_COLOR_MATRIX changes weren't recognized by teximage functions
      - glCopyPixels with scale and bias was broken
      - glRasterPos with lighting could segfault
      - glDeleteTextures could leave a dangling pointer
      - Proxy textures for cube maps didn't work
      - fixed a number of 16-bit color channel bugs
      - fixed a few minor memory leaks
      - GLX context sharing was broken in 3.5
      - fixed state-update bugs in glPopClientAttrib()
      - fixed glDrawRangeElements() bug
      - fixed a glPush/PopAttrib() bug related to texture binding
      - flat-shaded, textured lines were broken
      - fixed a dangling pointer problem in the XMesa code (Chris Burghart)
      - lighting didn't always produce the correct alpha value
      - fixed 3DNow! code to not read past end of arrays (Andrew Lewycky)
```
