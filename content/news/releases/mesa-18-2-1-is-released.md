---
title:    "Mesa 18.2.1 is released"
date:     2018-09-21 00:00:00
category: releases
tags:     []
---
[Mesa 18.2.1](https://docs.mesa3d.org/relnotes/18.2.1.html) is released. This is a bug-fix
release.
