---
title:    "Mesa 12.0.4 is released"
date:     2016-11-10 00:00:00
category: releases
tags:     []
---
[Mesa 12.0.4](https://docs.mesa3d.org/relnotes/12.0.4.html) is released. This is a bug-fix
release.
