---
title:    "Mesa is now using git as its source code management system"
date:     2006-12-05 00:00:00
category: misc
tags:     []
---
Mesa is now using git as its source code management system. The previous
CVS repository should no longer be used. See the [repository
page](repository.html) for more information.
